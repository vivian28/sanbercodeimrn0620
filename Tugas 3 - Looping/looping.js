console.log("No. 1 Looping While")
console.log("LOOPING PERTAMA")
var angka = 0
while(angka < 20) {
    angka += 2
    console.log(angka + ' - I love coding')
}
console.log("LOOPING KEDUA")
var angka = 22
while(angka > 2) {
    angka -= 2
    console.log(angka + ' - I will become a mobile developer')
}
console.log("=================================================")
console.log("No. 2 Looping menggunakan for")
for(i = 1; i < 21; i++) {
    if(i%2 == 0) {
        console.log(i + " - Berkualitas")
    } else {
        if(i%3 == 0) {
            console.log(i + ' - I Love Coding')
        } else {
            console.log(i + ' - Santai')
        }
    }
}
console.log("=================================================")
console.log("No. 3 Membuat Persegi Panjang #")
for(i = 1; i < 5; i++) {
    console.log("########")
}
console.log("=================================================")
console.log("No. 4 Membuat Tangga")
var pagar = ""
for(i = 1; i < 8; i++) {
    console.log(pagar += "#")
}
console.log("=================================================")
console.log("No. 5 Membuat Papan Catur")
var catur = "# # # # "
for(i = 1; i < 9; i++) {
    if(i%2 == 0) {
        console.log(catur)
    } else {
        console.log(" " + catur)
    }
}