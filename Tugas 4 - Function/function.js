console.log("No. 1")
function teriak() {
    console.log("Halo Sanbers!")
}

teriak()

console.log("No. 2")
function multiply(num1, num2) {
    return num1 * num2
}
var hasilKali = multiply(12, 4)
console.log(hasilKali)

console.log("No. 3")
function introduce(name = "Agus", age = 30, address = "Jln. Malioboro, Yogyakarta", hobby = "Gaming") {
    return ("Nama saya " + name +  ", umur saya " + age + " tahun, alamat saya di " + address + ", dan saya punya hobby yaitu " + hobby + "!")
}
var perkenalan = introduce()
console.log(perkenalan)