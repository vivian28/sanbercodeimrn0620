console.log("1. Animal Class")
console.log("Release 0")
class Animal {
    constructor(name) {
        this.name = name
        this.legs = 4
        this.cold_blooded = "false"
    }
}
var sheep = new Animal("shaun")
console.log(sheep.name)
console.log(sheep.legs)
console.log(sheep.cold_blooded)
console.log("Release 1")
class Ape extends Animal {
    constructor(name) {
        super(name)
    }
    yell() {
        return "Auooo"
    }
}
class Frog extends Animal {
    constructor(name) {
        super(name)
    }
    jump() {
        return "hop hop"
    }
}
var sungokong = new Ape("kera sakti")
console.log(sungokong.yell())
var kodok = new Frog("buduk")
console.log(kodok.jump())
console.log("==============================================")
console.log("No.2 Function to Class")
class Clock {
    constructor({template}) {
        this.template = template
    }
    render() {
        this.date = new Date();
        this.hours = this.date.getHours()
        if (this.hours < 10) this.hours = '0' + this.hours
        this.mins = this.date.getMinutes()
        if (this.mins < 10) this.mins = '0' + this.mins
        this.secs = this.date.getSeconds()
        if (this.secs < 10) this.secs = '0' + this.secs
        var output = this.template.replace('h', this.hours).replace('m', this.mins).replace('s', this.secs)
        console.log(output)
    }
    stop() {
        clearInterval(timer)
    }
    start() {
        return setInterval(this.render.bind(this), 1000)
    }
}
var clock = new Clock({template: 'h:m:s'})
clock.start()