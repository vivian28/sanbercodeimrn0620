console.log("No. 1 (Array to Object)")
var now = new Date()
var thisYear = now.getFullYear()
function arrayToObject(arr) {
    for(var i = 0; i < arr.length; i++){
        function umur() {
            if(arr[i][3] == undefined){
                return "Invalid birth year"
            }else if(arr[i][3] > thisYear){
                return "Invalid birth  year"
            }else{
                return(thisYear - arr[i][3])
            }
        }
        var objek = {
            firstName: arr[i][0],
            lastName: arr[i][1],
            gender: arr[i][2],
            age: umur()
        }
        console.log((i+1) + ". " + arr[i][0] + " " + arr[i][1] + ": ", objek)
    }
}
var people = [
    ["Bruce", "Banner", "male", 1975],
    ["Natasha", "Romanoff", "female"]
]
arrayToObject(people)
var people2 = [
    ["Tony", "Stark", "male", 1980],
    ["Pepper", "Pots", "female", 2023]
]
arrayToObject(people2)
console.log("==============================================")
console.log("No. 2 (Shopping Time)")
function shoppingTime(memberId, money) {
    if(memberId == undefined || memberId == "") {
        return "Mohon maaf, toko X hanya berlaku untuk member saja"
    }else if(money < 50000) {
        return "Mohon maaf, uang tidak cukup"
    }else{
        var belanjaan = []
        var kembalian = money
        if(money >= 1500000){
            belanjaan.push("Sepatu Stacattu")
            kembalian -= 1500000
        }
        if(money >= 500000){
            belanjaan.push("Baju Zoro")
            kembalian -= 500000
        }
        if(money >= 250000){
            belanjaan.push("Baju H&N")
            kembalian -= 250000
        }
        if(money >= 175000){
            belanjaan.push("Sweater Uniklooh")
            kembalian -= 175000
        }
        if(money >= 50000){
            belanjaan.push("Casing Handphone")
            kembalian -= 50000
        }
        var harga = {
            memberId: memberId,
            money: money,
            listPurchased: belanjaan,
            changeMoney: kembalian
        }
    }
    return harga
}
console.log(shoppingTime('1820RzKrnWn08', 2475000))
console.log(shoppingTime('82Ku8Ma742', 170000))
console.log(shoppingTime('', 2475000))
console.log(shoppingTime('234JdhweRxa53', 15000))
console.log(shoppingTime())
console.log("==============================================")
console.log("No. 3 (Naik Angkot)")
  function naikAngkot(arrPenumpang) {
    rute = ['A', 'B', 'C', 'D', 'E', 'F'];
    var arr = []
    for(j = 0; j < arrPenumpang.length; j++){
        function total() {
            function range(start, finish) {
                var result=[]
                var end=finish.charCodeAt(0)
                for (k=start.charCodeAt(0); k <=end; k++){
                    result.push(String.fromCharCode(k));
                }
                return result;
            }
            var up = arrPenumpang[j][1]
            var down = arrPenumpang[j][2]
            var huruf = (range(up, down))
            var minus = (huruf.length -= 1)
            var total = (minus * 2000)
            return total
        }
        var passenger = {
            penumpang: arrPenumpang[j][0],
            naikDari: arrPenumpang[j][1],
            tujuan: arrPenumpang[j][2],
            bayar: total()
        }
        arr.push(passenger)
    }
    return arr
}
console.log(naikAngkot([
    ['Dimitri', 'B', 'F'],
    ['Icha', 'A', 'B']
]))
console.log(naikAngkot([]))