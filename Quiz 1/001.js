console.log("A. Balik Kata")
function balikKata(str) {
    var currentString = str
    var newString = ""
    for (var i = str.length - 1; i >= 0; i--) {
        newString = newString + currentString[i];
 }
 return newString;
}
console.log(balikKata("abcde"))
console.log(balikKata("rusak"))
console.log(balikKata("racecar"))
console.log(balikKata("haji"))
console.log("====================================================")
console.log("B. Palindrome")
function palindrome(str) {
    var pln = str.length;
    for (var j = 0; j < pln/2; j++) {
      if (str[j] !== str[pln - 1 - j]) {
          return false;
      }
    }
    return true;
}
console.log(palindrome("kasur rusak")) // true
console.log(palindrome("haji ijah")) // true
console.log(palindrome("nabasan")) // false
console.log(palindrome("nababan")) // true
console.log(palindrome("jakarta"))
console.log("====================================================")
console.log("C. Bandingkan Angka")
function bandingkan(num1, num2) {
    if(num1 < 0 || num2 < 0) {
        return -1
    }else if(num2 > num1) {
        return num2
    }else if(num1 == num2) {
        return -1
    }else if(!num1 || !num2) {
        return num1
    }else{
        return -1
    }
} 
console.log(bandingkan(10, 15));
console.log(bandingkan(12, 12));
console.log(bandingkan(-1, 10));
console.log(bandingkan(112, 121));
console.log(bandingkan(1))
console.log(bandingkan())
console.log(bandingkan("15", "18"));