console.log(" A. Ascending Ten")
function AscendingTen(num) {
    if(num === undefined) {
        return "-1"
    }
    var angka = []
    var last = (num + 10)
    for (i = num; i < last; i++) {
        angka.push(i)
    }
    return angka.toString()
}

console.log(AscendingTen(11))
console.log(AscendingTen(21))
console.log(AscendingTen())
console.log("====================================================")
console.log("B. Descending Ten")
function DescendingTen(num2) {
    if(num2 === undefined) {
        return "-1"
    }
    var angka2 = []
    var last2 = (num2 - 10)
    for (j = num2; j > last2; j--) {
        angka2.push(j)
    }
    return angka2.toString()
}
console.log(DescendingTen(100))
console.log(DescendingTen(10))
console.log(DescendingTen())
console.log("====================================================")
console.log("C. Conditional Ascending Descending")
function ConditionalAscDesc(reference, check) {
    if(reference === undefined || check === undefined) {
        return "-1"
    }else if(check%2 == 0) {
        var angka3 = []
        var last3 = (reference - 10)
        for (k = reference; k > last3; k--) {
            angka3.push(k)
        }
        return angka3.toString()
    }else{
        var angka4 = []
        var last4 = (reference + 10)
        for (l = reference; l < last4; l++) {
            angka4.push(l)
        }
        return angka4.toString()
    }
}
console.log(ConditionalAscDesc(20, 8))
console.log(ConditionalAscDesc(81, 1))
console.log(ConditionalAscDesc(31))
console.log(ConditionalAscDesc())
console.log("====================================================")
console.log("D. Papan Ular Tangga")
function ularTangga() {
    console.log(DescendingTen(100))
    console.log(AscendingTen(81))
    console.log(DescendingTen(80))
    console.log(AscendingTen(61))
    console.log(DescendingTen(60))
    console.log(AscendingTen(41))
    console.log(DescendingTen(40))
    console.log(AscendingTen(21))
    console.log(DescendingTen(20))
    console.log(AscendingTen(1))
}

console.log(ularTangga())