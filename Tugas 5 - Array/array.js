console.log("Soal No. 1 (Range)")
function range(startNum, finishNum) {
    if(!startNum || !finishNum) {
        return -1;
    }
    var angka = []
    for (i = startNum; i <= finishNum; i++) {
        angka.push(i)
    }
    for(i = startNum; i >= finishNum; i--) {
        angka.push(i)
    }
    return angka
}

console.log(range(1, 10))
console.log(range(1))
console.log(range(11,18))
console.log(range(54, 50))
console.log(range())

console.log("============================================")
console.log("Soal No. 2 (Range with Step)")

function rangeWithStep(startNum, finishNum, step) {
    var number = []
    for (i = startNum; i <= finishNum; i += step) {
        number.push(i)
    }
    for (i = startNum; i >= finishNum; i -= step) {
        number.push(i)
    }
    return number
}

console.log(rangeWithStep(1, 10, 2))
console.log(rangeWithStep(11, 23, 3))
console.log(rangeWithStep(5, 2, 1))
console.log(rangeWithStep(29, 2, 4))

console.log("============================================")
console.log("Soal No. 3 (Sum of Range)")

function sum(angkaAwal, angkaAkhir, step) {
    if(!angkaAwal || !angkaAkhir) {
        return angkaAwal
    } else if(angkaAwal === undefined || angkaAkhir === undefined) {
        return 0
    }
    var sumAngka = 0
    if(step === undefined) {
        numAngka = range(angkaAwal, angkaAkhir)
    } else{
        numAngka = rangeWithStep(angkaAwal, angkaAkhir, step)
    }
    for(j = 0; j < numAngka.length; j++){
        sumAngka += numAngka[j]
    }
    return sumAngka
}

console.log(sum(1,10))
console.log(sum(5, 50, 2))
console.log(sum(15,10))
console.log(sum(20, 10, 2))
console.log(sum(1))
console.log(sum(0))

console.log("============================================")
console.log("Soal No. 4 (Array Multidimensi)")

var input = [
    ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
    ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
    ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
    ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
]

console.log("Nomor ID: " + input[0][0])
console.log("Nama Lengkap: " + input[0][1])
console.log("TTL: " + input[0][2] + " " + input[0][3])
console.log("Nomor ID: " + input[0][4])
console.log(" ")
console.log("Nomor ID: " + input[1][0])
console.log("Nama Lengkap: " + input[1][1])
console.log("TTL: " + input[1][2] + " " + input[1][3])
console.log("Nomor ID: " + input[1][4])
console.log(" ")
console.log("Nomor ID: " + input[2][0])
console.log("Nama Lengkap: " + input[2][1])
console.log("TTL: " + input[2][2] + " " + input[2][3])
console.log("Nomor ID: " + input[2][4])
console.log(" ")
console.log("Nomor ID: " + input[3][0])
console.log("Nama Lengkap: " + input[3][1])
console.log("TTL: " + input[3][2] + " " + input[3][3])
console.log("Nomor ID: " + input[3][4])

console.log("============================================")
console.log("Soal No. 5 (Balik Kata)")

function balikKata(str) {
    var currentString = str
    var newString = ""
    for (let i = str.length - 1; i >= 0; i--) {
        newString = newString + currentString[i];
 }
 return newString;
}

console.log(balikKata("Kasur Rusak")) // kasuR rusaK
console.log(balikKata("SanberCode")) // edoCrebnaS
console.log(balikKata("Haji Ijah")) // hajI ijaH
console.log(balikKata("racecar")) // racecar
console.log(balikKata("I am Sanbers"))

console.log("============================================")
console.log("Soal No. 6 (Metode Array)")

function dataHandling () {
    var input = ["0001", "Roman Alamsyah Elsharawy", "Provinsi Bandar Lampung", "21/05/1989", "Membaca"]
    input.splice(4, 1, "Pria", "SMA Internasional Metro")
    console.log(input)
    var tanggal = input[0, 3]
    var newtanggal = tanggal.split("/")
    var newtanggal1 = tanggal.split("/")
    var bulan = 05
    switch (bulan) {
        case 01:
            console.log("Januari")
            break; 
        case 02:
            console.log("Februari")
            break;
        case 03:
            console.log("Maret")
            break;
        case 04:
            console.log("April")
            break;
        case 5:
            console.log("Mei")
            break;
        case 06:
            console.log("Juni")
            break;
        case 07:
            console.log("Juli")
            break;
        case 08:
            console.log("Agustus")
            break;
        case 09:
            console.log("September")
            break;
        case 10:
            console.log("Oktober")
            break;
        case 11:
            console.log("November")
            break;
        case 12:
            console.log("Desember")
            break;
        }
    var sorted = newtanggal.sort((a,b)=>b-a)
    console.log(sorted)
    var joining = newtanggal1.join("-")
    console.log(joining)
    var name = input[0,1]
    var newname = name.slice(0, 14)
    console.log(newname)
}

dataHandling()